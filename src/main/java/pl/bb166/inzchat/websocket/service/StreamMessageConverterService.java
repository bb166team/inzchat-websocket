package pl.bb166.inzchat.websocket.service;


import com.google.common.reflect.ClassPath;
import com.google.gson.Gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.websocket.domain.message.Message;
import pl.bb166.inzchat.websocket.util.Tuple;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StreamMessageConverterService implements MessageConverterService {
    static Logger logger = LoggerFactory.getLogger(StreamMessageConverterService.class);

    private List<Tuple<String, Class<?>>> classes;

    private Gson gson;

    private Reflections reflections;

    @Autowired
    public void setGson(Gson gson) {
        this.gson = gson;
    }

    @Value("${inzChat.websocket.messagePath}")
    public void init(String messagesPackagePath) {
        try {
            List<ClassLoader> classLoadersList = new LinkedList<>();
            classLoadersList.add(ClasspathHelper.contextClassLoader());
            classLoadersList.add(ClasspathHelper.staticClassLoader());


            this.reflections = new Reflections(new ConfigurationBuilder()
                    .setScanners(new SubTypesScanner(false), new ResourcesScanner())
                    .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                    .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(messagesPackagePath))));

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        this.classes = getAllMessageTypes();
    }

    private List<Tuple<String, Class<?>>> getAllMessageTypes() {
        return reflections.getSubTypesOf(Object.class)
                .stream()
                .map(
                        element ->
                                Tuple.<String, Class<?>>tuple(element.getSimpleName(), element)

                )
                .collect(
                        Collectors.toList()
                );
    }

    @Override
    public Message convertMessageFromJson(String jsonMessage) {
        String messageType = gson.fromJson(jsonMessage, Message.class).getMessageType();
        return classes
                .stream()
                .filter(element -> messageType.equals(element._1))
                .map(element ->
                        (Message) gson.fromJson(jsonMessage, element._2)

                )
                .findAny()
                .orElse(new Message());
    }

    @Override
    public String convertMessageToJson(Message message) {
        return classes
                .stream()
                .filter(element -> element._2 == message.getClass())
                .map(element ->  {
                    JsonElement objectNode = gson.toJsonTree(message);
                    objectNode.getAsJsonObject().add("messageType", new JsonPrimitive(element._1));
                    return objectNode.toString();
                })
                .findAny()
                .orElse("{}");
    }
}
