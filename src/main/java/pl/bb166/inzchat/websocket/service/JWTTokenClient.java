package pl.bb166.inzchat.websocket.service;

import com.google.gson.Gson;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.websocket.domain.dto.JWTTokenDTO;

import java.io.IOException;

@Service
public class JWTTokenClient {
    static Logger logger = LoggerFactory.getLogger(JWTTokenClient.class);

    @Value("${inzChat.websocket.component.auth.grantType}")
    private String grantType;

    @Value("${inzChat.websocket.component.auth.proxyUrl}")
    private String authProxy;

    @Value("${inzChat.websocket.component.auth.proxyPort}")
    private String port;

    @Value("${inzChat.websocket.component.auth.tokenEndpoint}")
    private String tokenEndpoint;

    private OkHttpClient jwtClient;

    private Gson gson;

    @Autowired
    public void setJwtClient(OkHttpClient jwtClient) {
        this.jwtClient = jwtClient;
    }

    @Autowired
    public void setGson(Gson gson) {
        this.gson = gson;
    }

    public JWTTokenDTO getAccessToken(String userName, String password) {

        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("grant_type", grantType)
                    .addFormDataPart("username", userName)
                    .addFormDataPart("password", password)
                    .build();

            Request request = new Request.Builder()
                    .url("http://"+ authProxy+ ":"+ port+ tokenEndpoint)
                    .post(requestBody)
                    .build();

            return gson.fromJson(jwtClient.newCall(request).execute().body().string(), JWTTokenDTO.class);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }

        return null;
    }
}
