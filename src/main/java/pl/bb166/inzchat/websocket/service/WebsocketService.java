package pl.bb166.inzchat.websocket.service;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bb166.inzchat.websocket.actor.Communication;
import pl.bb166.inzchat.websocket.domain.message.Logout;
import pl.bb166.inzchat.websocket.domain.message.request.RegisterConnection;
import pl.bb166.inzchat.websocket.domain.message.Message;
import pl.bb166.inzchat.websocket.util.Tuple;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class WebsocketService extends WebSocketServer {
    static Logger logger = LoggerFactory.getLogger(WebsocketService.class);

    private ActorSystem actorSystem;

    private ActorRef root;

    private MessageConverterService messageConverterService;

    private Map<WebSocket, ActorRef> connections = new ConcurrentHashMap<>();

    private ClientsService clientsService;

    private MappersService mappersService;

    public WebsocketService(InetSocketAddress port,
                            ActorSystem actorSystem,
                            ActorRef root,
                            MessageConverterService messageConverterService,
                            ClientsService clientsService,
                            MappersService mappersService) {
        super(port);
        this.actorSystem = actorSystem;
        this.messageConverterService = messageConverterService;
        this.root = root;
        this.clientsService = clientsService;
        this.mappersService = mappersService;
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        ActorRef actorRef = actorSystem.actorOf(Communication.props(root, webSocket, messageConverterService, clientsService, mappersService));
        root.tell(new RegisterConnection(), actorRef);
        connections.put(webSocket, actorRef);
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        ActorRef actorRef = connections.remove(webSocket);
        actorRef.tell(new Logout(), ActorRef.noSender());
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        ActorRef actorRef = connections.get(webSocket);
        Message message = messageConverterService.convertMessageFromJson(s);
        actorRef.tell(message, ActorRef.noSender());
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {

    }

    @Override
    public void onStart() {

    }
}
