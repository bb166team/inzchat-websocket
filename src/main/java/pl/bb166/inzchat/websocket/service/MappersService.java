package pl.bb166.inzchat.websocket.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.websocket.domain.mapper.MessageMapper;
import pl.bb166.inzchat.websocket.domain.mapper.RegisterRequestMapper;
import pl.bb166.inzchat.websocket.domain.mapper.RegisterResponseMapper;

@Service
public class MappersService {
    @Autowired
    @Getter @Setter
    private RegisterRequestMapper registerRequestMapper;

    @Autowired
    @Getter @Setter
    private RegisterResponseMapper registerResponseMapper;

    @Autowired
    @Getter @Setter
    private MessageMapper messageMapper;
}
