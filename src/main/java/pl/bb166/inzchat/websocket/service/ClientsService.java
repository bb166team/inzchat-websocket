package pl.bb166.inzchat.websocket.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.websocket.client.Auth;
import pl.bb166.inzchat.websocket.client.Message;

@Service
public class ClientsService {
    @Autowired
    @Getter
    private Auth auth;

    @Autowired
    @Getter
    private Message message;

    @Autowired
    @Getter
    private JWTTokenClient jwtTokenClient;
}
