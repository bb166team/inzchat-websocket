package pl.bb166.inzchat.websocket.service;

import pl.bb166.inzchat.websocket.domain.message.Message;
import pl.bb166.inzchat.websocket.util.Tuple;

public interface MessageConverterService {
    Message convertMessageFromJson(String jsonMessage);
    String convertMessageToJson(Message message);
}
