package pl.bb166.inzchat.websocket.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bb166.inzchat.websocket.domain.PrivateDTO;
import pl.bb166.inzchat.websocket.domain.dto.CreatePrivateMessageDTO;
import pl.bb166.inzchat.websocket.domain.message.Logout;
import pl.bb166.inzchat.websocket.domain.message.UserRegisteredInPrivate;
import pl.bb166.inzchat.websocket.domain.message.request.PrivateSendMessageRequest;
import pl.bb166.inzchat.websocket.domain.message.response.*;
import pl.bb166.inzchat.websocket.service.ClientsService;
import pl.bb166.inzchat.websocket.service.MappersService;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

public class Private extends AbstractActor {
    static Logger logger = LoggerFactory.getLogger(Private.class);

    private String firstUsername;

    private String secondUsername;

    private PrivateDTO privateDTO;

    private ActorRef firstUser;

    private ActorRef secondUser;

    private MappersService mappersService;

    private ClientsService clientsService;

    public Private(PrivateDTO privateDTO, ClientsService clientsService, MappersService mappersService) {
        this.firstUsername = privateDTO.getFirstUser();
        this.secondUsername = privateDTO.getSecondUser();
        this.clientsService = clientsService;
        this.mappersService = mappersService;
        this.privateDTO = privateDTO;
    }

    public static Props props(PrivateDTO privateDTO, ClientsService clientsService, MappersService mappersService) {
        return Props.create(Private.class, privateDTO, clientsService, mappersService);
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()

                .match(RegisterPrivateUser.class, this::onRegisterUser)

                .match(RegisterUserWithNotify.class, this::onRegisterUserWithNotify)

                .match(PrivateSendMessageRequest.class, this::onPrivateSendMessageRequest)

                .match(Logout.class, this::onLogout)

                .match(UploadFileResponse.class, this::onUploadFileResponse)

                .build();
    }

    private void onRegisterUser(RegisterPrivateUser msg) {
        if (firstUser != null)
            firstUser.tell(msg, ActorRef.noSender());
        if (secondUser != null)
            secondUser.tell(msg, ActorRef.noSender());

        if (firstUsername.equals(msg.getUsername())) {
            firstUser = getSender();
        } else {
            secondUser = getSender();
        }
        getSender().tell(new UserRegisteredInPrivate(privateDTO), getSelf());
    }

    private void onRegisterUserWithNotify(RegisterUserWithNotify msg) {
        if (firstUsername.equals(msg.getSender())){
            firstUser = msg.getSenderActor();
            firstUser.tell(new NotifyUser(secondUsername), ActorRef.noSender());
            if (msg.getReceiver() != null) {
                secondUser = msg.getReceiverActor();
                secondUser.tell(new NotifyUser(firstUsername), ActorRef.noSender());
            }
        } else {
            secondUser = msg.getSenderActor();
            secondUser.tell(new NotifyUser(firstUsername), ActorRef.noSender());
            if(msg.getReceiver() != null) {
                firstUser = msg.getReceiverActor();
                firstUser.tell(new NotifyUser(secondUsername), ActorRef.noSender());
            }
        }
    }

    private void onUploadFileResponse(UploadFileResponse msg) {
        msg.setOwners(Arrays.asList(firstUsername, secondUsername));
        getSender().tell(msg, ActorRef.noSender());

    }

    private void onPrivateSendMessageRequest(PrivateSendMessageRequest msg) {
        MessageResponse.MessageResponseBuilder builder = MessageResponse.builder()
                .messages(Collections.singletonList(mappersService.getMessageMapper().privateSendMessageRequestToMessageDTO(msg)))
                .position(Position.START);



        if (firstUser != null) {
            MessageResponse msgRes = builder.groupName(secondUsername).build();
            msgRes.setComponentName("MessageComponent");
            firstUser.tell(msgRes, ActorRef.noSender());
        }

        if (secondUser != null) {
            MessageResponse msgRes = builder.groupName(firstUsername).build();
            msgRes.setComponentName("MessageComponent");
            secondUser.tell(msgRes, ActorRef.noSender());
        }

        CreatePrivateMessageDTO createPrivateMessageDTO =
                mappersService.getMessageMapper().privateSendMessageRequestToCreatePrivateMessageDTO(msg);

        CompletableFuture.runAsync(() -> {
                    clientsService.getMessage().createPrivateMessage(msg.getJwtTokenDTO().getAccessToken(), createPrivateMessageDTO);
                }
        );
    }

    private void onLogout(Logout msg) {
        if (msg.getUsername().equals(firstUsername)) {
            firstUser = null;
            if (secondUser != null)
                secondUser.tell(new LogoutResponse(firstUsername), ActorRef.noSender());
        }

        if (msg.getUsername().equals(secondUsername)) {
            secondUser = null;
            if (firstUser != null) {
                firstUser.tell(new LogoutResponse(secondUsername), ActorRef.noSender());
            }
        }
    }
}
