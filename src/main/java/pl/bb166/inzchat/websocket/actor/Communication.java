package pl.bb166.inzchat.websocket.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import org.java_websocket.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bb166.inzchat.websocket.client.Auth;
import pl.bb166.inzchat.websocket.client.Message;
import pl.bb166.inzchat.websocket.domain.PrivateDTO;
import pl.bb166.inzchat.websocket.domain.dto.*;
import pl.bb166.inzchat.websocket.domain.message.Logout;
import pl.bb166.inzchat.websocket.domain.message.UserRegisteredInGroup;
import pl.bb166.inzchat.websocket.domain.message.UserRegisteredInPrivate;
import pl.bb166.inzchat.websocket.domain.message.request.*;
import pl.bb166.inzchat.websocket.domain.message.response.*;
import pl.bb166.inzchat.websocket.service.ClientsService;
import pl.bb166.inzchat.websocket.service.MappersService;
import pl.bb166.inzchat.websocket.service.MessageConverterService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Communication extends AbstractActor {
    private static final Logger logger = LoggerFactory.getLogger(Communication.class);

    private WebSocket out;

    private JWTTokenDTO jwtTokenDTO;

    private MessageConverterService messageConverterService;

    private ClientsService clientsService;

    private MappersService mappersService;

    private ActorRef root;

    private String username;

    private Map<String, ActorRef> groups = new HashMap<>();

    private Map<PrivateDTO, ActorRef> users = new HashMap<>();

    public Communication(ActorRef root,
                         WebSocket out,
                         MessageConverterService messageConverterService,
                         ClientsService clientsService,
                         MappersService mappersService) {
        this.root = root;
        this.out = out;
        this.messageConverterService = messageConverterService;
        this.clientsService = clientsService;
        this.mappersService = mappersService;
    }

    public static Props props(ActorRef root,
                              WebSocket out,
                              MessageConverterService messageConverterService,
                              ClientsService clientsService,
                              MappersService mappersService) {
        return Props.create(Communication.class, root, out, messageConverterService, clientsService, mappersService);
    }

    private Receive loggedBehavior = ReceiveBuilder.create()

            .match(RefreshMenuRequest.class, this::onLoginSuccess)

            .match(SearchRequest.class, this::onSearchRequest)

            .match(GroupMessageRequest.class, this::onGroupMessageRequest)

            .match(GroupSendMessageRequest.class, this::onGroupSendMessageRequest)

            .match(UserRegisteredInGroup.class, this::onUserRegisteredInGroup)

            .match(UserRegisteredInPrivate.class, this::onUserRegisterInPrivate)

            .match(MessageResponse.class, this::onMessageResponse)

            .match(AddUserToGroupRequest.class, this::onAddUserToGroup)

            .match(PrivateMessageRequest.class, this::onPrivateMessageRequest)

            .match(PrivateSendMessageRequest.class, this::onPrivateSendMessageRequest)

            .match(NotifyUser.class, this::onNotifyUser)

            .match(Logout.class, this::onLogout)

            .match(SearchResponse.class, this::onSearchResponse)

            .match(LogoutResponse.class, this::onLogoutResponse)

            .match(RegisterPrivateUser.class, this::onRegisterPrivateUser)

            .match(AddGroupRequest.class, this::onAddGroupRequest)

            .match(PushUserToGroup.class, this::onPushUserToGroup)

            .match(NotifyGroup.class, this::onNotifyGroup)

            .match(UploadFileRequest.class, this::onUploadFileRequest)

            .match(UploadFileResponse.class, this::onUploadFileResponse)

            .match(DownloadFileRequest.class, this::onDownloadFileRequest)

            .build();

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()

                .match(ConnectionSuccessful.class, this::onConnectionSuccessful)

                .match(LoginRequest.class, this::onLogin)

                .match(RegisterUserRequest.class, this::onRegister)

                .match(Logout.class, this::onLogout)

                .build();
    }

    private void onNotifyGroup(NotifyGroup msg) {
        msg.setComponentName("MenuComponent");
        out.send(messageConverterService.convertMessageToJson(msg));
    }

    private void onAddUserToGroup(AddUserToGroupRequest msg) {
        Auth auth = clientsService.getAuth();
        auth.addUserToGroup(jwtTokenDTO.getAccessToken(), msg.getGroupName());

        msg.setUserName(username);
        root.tell(msg, getSelf());
    }

    private void onUserRegisterInPrivate(UserRegisteredInPrivate msg) {
        users.put(msg.getPrivateDTO(), getSender());
    }

    private void onUserRegisteredInGroup(UserRegisteredInGroup msg) {
        groups.put(msg.getName(), getSender());
    }

    private void onRegister(RegisterUserRequest registerUserRequest) {
        Auth auth = clientsService.getAuth();

        RegisterUserValidationDTO registerUserValidationDTO = auth.registerNewUser(mappersService
                .getRegisterRequestMapper()
                .registerRequestToRegisterUserDTO(registerUserRequest));

        RegisterUserResponse registerUserResponse = mappersService.getRegisterResponseMapper().registerUserValidationDTOToRegisterRequest(registerUserValidationDTO);

        registerUserResponse.setComponentName(registerUserRequest.getComponentName());

        out.send(messageConverterService.convertMessageToJson(registerUserResponse));
    }

    private void onLogin(LoginRequest loginRequest) {
        JWTTokenDTO jwtTokenDTO = clientsService.getJwtTokenClient().getAccessToken(
                loginRequest.getUsername(),
                loginRequest.getPassword()
        );
        LoginResponse loginResponse = LoginResponse.builder()
                    .loginStatus(
                            jwtTokenDTO.getAccessToken() != null ?
                            LoginResponse.LoginStatus.SUCCESSFUL :
                            LoginResponse.LoginStatus.FILED
                    )
                    .build();

        loginResponse.setComponentName(loginRequest.getComponentName());

        if (loginResponse.getLoginStatus() == LoginResponse.LoginStatus.SUCCESSFUL) {
            ifLoginSuccessful(loginRequest, jwtTokenDTO);
        }

        out.send(
                messageConverterService.convertMessageToJson(loginResponse)
        );

        if (loginResponse.getLoginStatus() == LoginResponse.LoginStatus.SUCCESSFUL)
            getSelf().tell(new RefreshMenuRequest(), ActorRef.noSender());

    }

    private void ifLoginSuccessful(LoginRequest loginRequest, JWTTokenDTO jwtTokenDTO) {
        List<RoomDTO> rooms = clientsService.getAuth().rooms(jwtTokenDTO.getAccessToken());
        List<UserDTO> users = clientsService.getAuth().getAllUsers(jwtTokenDTO.getAccessToken());

        root.tell(new LoginSuccessfulResponse(loginRequest.getUsername(), users, rooms), getSelf());
        this.username = loginRequest.getUsername();
        this.jwtTokenDTO = jwtTokenDTO;
        getContext().become(loggedBehavior);
    }

    private void onConnectionSuccessful(ConnectionSuccessful connectionSuccessful) {
        out.send(messageConverterService.convertMessageToJson(connectionSuccessful));
    }

    private void onLoginSuccess(RefreshMenuRequest refreshMenuRequest) {
        List<RoomDTO> rooms = clientsService.getAuth().rooms(jwtTokenDTO.getAccessToken());
        List<UserDTO> users = clientsService.getAuth().getAllUsers(jwtTokenDTO.getAccessToken());

        SearchResponse searchResponse = new SearchResponse(rooms, users);
        searchResponse.setComponentName("MenuComponent");

        root.tell(searchResponse, getSelf());
    }

    private void onSearchRequest(SearchRequest searchRequest) {
        Auth auth = clientsService.getAuth();

        List<RoomDTO> roomsResult = auth.searchRooms(jwtTokenDTO.getAccessToken(), searchRequest.getQuery());
        List<UserDTO> usersResult = auth.searchUsers(jwtTokenDTO.getAccessToken(), searchRequest.getQuery());

        SearchResponse searchResponse = new SearchResponse(roomsResult, usersResult);
        searchResponse.setComponentName("MenuComponent");

        root.tell(searchResponse, getSelf());
    }

    private void onGroupMessageRequest(GroupMessageRequest groupMessageRequest) {
        Message message = clientsService.getMessage();

        List<MessageDTO> messageResult = message.getMessagesForGroup(jwtTokenDTO.getAccessToken(), groupMessageRequest.getGroupName());

        MessageResponse messageResponse = new MessageResponse(groupMessageRequest.getGroupName(), messageResult, Position.START);

        messageResponse.setComponentName(groupMessageRequest.getComponentName());

        out.send(messageConverterService.convertMessageToJson(messageResponse));
    }

    private void onPrivateMessageRequest(PrivateMessageRequest privateMessageRequest) {
        Message message = clientsService.getMessage();

        List<MessageDTO> messageResult = message.getPrivateMessagesForUsers(jwtTokenDTO.getAccessToken(), username, privateMessageRequest.getFirstUser());

        MessageResponse messageResponse = new MessageResponse(privateMessageRequest.getFirstUser(), messageResult, Position.START);

        messageResponse.setComponentName(privateMessageRequest.getComponentName());

        out.send(messageConverterService.convertMessageToJson(messageResponse));
    }

    private void onMessageResponse(MessageResponse msg) {
        out.send(messageConverterService.convertMessageToJson(msg));
    }

    private void onGroupSendMessageRequest(GroupSendMessageRequest groupSendMessageRequest) {
        groupSendMessageRequest.setAuthor(username);
        groupSendMessageRequest.setJwtTokenDTO(jwtTokenDTO);

        ActorRef group = groups.get(groupSendMessageRequest.getGroupName());
        group.tell(groupSendMessageRequest, getSelf());
    }

    private void onPrivateSendMessageRequest(PrivateSendMessageRequest privateSendMessageRequest) {
        PrivateDTO privateDTO = new PrivateDTO(privateSendMessageRequest.getReceiver(), username);

        ActorRef privateActor = users.get(privateDTO);
        privateSendMessageRequest.setJwtTokenDTO(jwtTokenDTO);
        privateSendMessageRequest.setSender(username);
        if (privateActor != null) {
            privateActor.tell(privateSendMessageRequest, ActorRef.noSender());
        } else {
            root.tell(privateSendMessageRequest,getSelf());
        }
    }

    private void onNotifyUser(NotifyUser notifyUser) {
        notifyUser.setComponentName("MenuComponent");
        out.send(messageConverterService.convertMessageToJson(notifyUser));
    }

    private void onLogout(Logout msg) {
        msg.setUsername(username);
        root.tell(msg, getSelf());
        users.forEach((a, b) -> b.tell(msg, getSelf()));
        groups.forEach((a, b) -> b.tell(msg, getSelf()));

        getContext().stop(getSelf());
    }

    private void onLogoutResponse(LogoutResponse msg) {
        msg.setComponentName("MenuComponent");
        out.send(messageConverterService.convertMessageToJson(msg));
    }

    private void onSearchResponse(SearchResponse msg) {
        out.send(messageConverterService.convertMessageToJson(msg));
    }

    private void onRegisterPrivateUser(RegisterPrivateUser msg) {
        msg.setComponentName("MenuComponent");
        out.send(messageConverterService.convertMessageToJson(msg));
    }

    private void onAddGroupRequest(AddGroupRequest msg) {
        clientsService.getAuth().addGroup(jwtTokenDTO.getAccessToken(), msg.getGroupName());
        clientsService.getAuth().addUserToGroup(jwtTokenDTO.getAccessToken(), msg.getGroupName());

        AddGroupSuccess addGroupSuccess = new AddGroupSuccess();
        addGroupSuccess.setComponentName("MenuComponent");
        addGroupSuccess.setGroupName(msg.getGroupName());

        out.send(messageConverterService.convertMessageToJson(addGroupSuccess));
    }

    private void onPushUserToGroup(PushUserToGroup msg) {
        clientsService.getAuth().addAnotherUserToGroup(jwtTokenDTO.getAccessToken(), msg.getGroupName(), msg.getUsername());

        root.tell(msg, ActorRef.noSender());
    }

    private void onUploadFileRequest(UploadFileRequest msg) {
        if (!msg.getGroupType().equals("private")) {
            UploadFileResponse response = new UploadFileResponse();
            response.setToken(jwtTokenDTO.getAccessToken());
            response.setComponentName(msg.getComponentName());

            out.send(messageConverterService.convertMessageToJson(response));
        } else {
            PrivateDTO privateDTO = new PrivateDTO(msg.getName(), username);

            ActorRef actorRef = users.get(privateDTO);

            if (actorRef != null) {
                UploadFileResponse uploadFileResponse = new UploadFileResponse();
                uploadFileResponse.setToken(jwtTokenDTO.getAccessToken());
                uploadFileResponse.setComponentName(msg.getComponentName());
                actorRef.tell(uploadFileResponse, getSelf());
            }
        }
    }

    private void onUploadFileResponse(UploadFileResponse msg) {
        out.send(messageConverterService.convertMessageToJson(msg));
    }

    private void onDownloadFileRequest(DownloadFileRequest msg) {
        DownloadFileResponse downloadFileResponse =
                DownloadFileResponse.builder()
                        .id(msg.getId())
                        .token(jwtTokenDTO.getAccessToken())
                        .build();

        downloadFileResponse.setComponentName(msg.getComponentName());
        out.send(messageConverterService.convertMessageToJson(downloadFileResponse));
    }
}