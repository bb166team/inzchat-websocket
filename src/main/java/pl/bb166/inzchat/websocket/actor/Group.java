package pl.bb166.inzchat.websocket.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bb166.inzchat.websocket.domain.dto.CreateGroupMessageDTO;
import pl.bb166.inzchat.websocket.domain.message.Logout;
import pl.bb166.inzchat.websocket.domain.message.RegisterGroupUser;
import pl.bb166.inzchat.websocket.domain.message.UserRegisteredInGroup;
import pl.bb166.inzchat.websocket.domain.message.request.GroupSendMessageRequest;
import pl.bb166.inzchat.websocket.domain.message.request.PushUserToGroup;
import pl.bb166.inzchat.websocket.domain.message.response.LogoutResponse;
import pl.bb166.inzchat.websocket.domain.message.response.MessageResponse;
import pl.bb166.inzchat.websocket.domain.message.response.Position;
import pl.bb166.inzchat.websocket.service.ClientsService;
import pl.bb166.inzchat.websocket.service.MappersService;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public class Group extends AbstractActor {
    static Logger logger = LoggerFactory.getLogger(Group.class);
    private ClientsService clientsService;

    private MappersService mappersService;

    private Set<ActorRef> users = new HashSet<>();

    private String name;

    public Group(String name, ClientsService clientsService, MappersService mappersService) {
        this.clientsService = clientsService;
        this.mappersService = mappersService;
        this.name = name;
    }

    public static Props props(String name, ClientsService clientsService, MappersService mappersService) {
        return Props.create(Group.class, name, clientsService, mappersService);
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()

                .match(RegisterGroupUser.class, this::onRegisterUser)

                .match(GroupSendMessageRequest.class, this::onSendMessageRequest)

                .match(PushUserToGroup.class, this::onPushUserToGroup)

                .match(Logout.class, this::onLogout)

                .build();
    }

    private void onPushUserToGroup(PushUserToGroup msg) {
        users.add(getSender());
    }

    private void onRegisterUser(RegisterGroupUser msg) {
        users.add(getSender());
        getSender().tell(new UserRegisteredInGroup(name), getSelf());
    }

    private void onSendMessageRequest(GroupSendMessageRequest msg) {
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessages(Collections.singletonList(mappersService.getMessageMapper().groupSendMessageRequestToMessageDTO(msg)));
        messageResponse.setComponentName("MessageComponent");
        messageResponse.setGroupName(name);
        messageResponse.setPosition(Position.START);

        users.forEach(usr ->
                usr.tell(messageResponse, ActorRef.noSender())
        );

        CreateGroupMessageDTO createGroupMessageDTO = mappersService.getMessageMapper().groupSendMessageRequestToCreateGroupMessageDTO(msg);

        CompletableFuture.runAsync(() -> {
                    clientsService.getMessage().createGroupMessage(msg.getJwtTokenDTO().getAccessToken(), createGroupMessageDTO);
                }
        );
    }

    private void onLogout(Logout msg) {
        int o = users.size();
        users.remove(getSender());
        users.forEach(user -> user.tell(new LogoutResponse(msg.getUsername()), ActorRef.noSender()));
    }
}
