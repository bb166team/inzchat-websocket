package pl.bb166.inzchat.websocket.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.bb166.inzchat.websocket.domain.PrivateDTO;
import pl.bb166.inzchat.websocket.domain.dto.RoomDTO;
import pl.bb166.inzchat.websocket.domain.dto.UserDTO;
import pl.bb166.inzchat.websocket.domain.message.Logout;
import pl.bb166.inzchat.websocket.domain.message.RegisterGroupUser;
import pl.bb166.inzchat.websocket.domain.message.request.*;
import pl.bb166.inzchat.websocket.domain.message.response.*;
import pl.bb166.inzchat.websocket.service.ClientsService;
import pl.bb166.inzchat.websocket.service.MappersService;

import java.util.*;

public class Root extends AbstractActor {
    static Logger logger = LoggerFactory.getLogger(Root.class);

    private Set<ActorRef> noAuthorizedConnections = new HashSet<>();

    private Map<String, ActorRef> authorizedConnections = new HashMap<>();

    private ClientsService clientsService;

    private MappersService mappersService;

    private Map<String, ActorRef> groups = new HashMap<>();

    private Map<PrivateDTO, ActorRef> privates = new HashMap<>();

    public Root(ClientsService clientsService, MappersService mappersService) {
        this.clientsService = clientsService;
        this.mappersService = mappersService;
    }

    public static Props props(ClientsService clientsService, MappersService mappersService) {
        return Props.create(Root.class, clientsService, mappersService);
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create()

                .match(RegisterConnection.class, this::onRegisterConnectionRequest)

                .match(LoginSuccessfulResponse.class, this::onLoginSuccessfulResponse)

                .match(AddUserToGroupRequest.class, this::registerUserInGroup)

                .match(PrivateSendMessageRequest.class, this::onPrivateSendMessageRequest)

                .match(SearchResponse.class, this::onSearchResponse)

                .match(Logout.class, this::onLogout)

                .match(PushUserToGroup.class, this::onPushUserToGroup)

                .build();
    }

    private void onPushUserToGroup(PushUserToGroup msg) {
        Optional<ActorRef> connection = authorizedConnections
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().equals(msg.getUsername())).findAny().map(Map.Entry::getValue);

        connection.ifPresent(val -> val.tell(new NotifyGroup(msg.getGroupName()), ActorRef.noSender()));

        connection.ifPresent(val ->

                groups.entrySet()
                        .stream()
                        .filter(entry -> entry.getKey().equals(msg.getGroupName()))
                        .forEach(entry -> entry.getValue().tell(msg, val))

        );
    }

    private void registerUserInGroup(AddUserToGroupRequest addUserToGroupRequest) {
        ActorRef groupActor = this.groups.get(addUserToGroupRequest.getGroupName());

        if (groupActor == null) {
            groupActor = getContext().actorOf(Group.props(addUserToGroupRequest.getGroupName(), clientsService, mappersService));
            this.groups.put(addUserToGroupRequest.getGroupName(), groupActor);
        }

        groupActor.tell(new RegisterGroupUser(addUserToGroupRequest.getGroupName()), getSender());
    }

    private void onLoginSuccessfulResponse(LoginSuccessfulResponse msg) {
        List<RoomDTO> groups = msg.getRooms();
        List<UserDTO> users = msg.getUsers();

        groups.forEach(room -> {
            ActorRef groupActor = this.groups.get(room.getName());

            if (groupActor == null) {
                groupActor = getContext().actorOf(Group.props(room.getName(), clientsService, mappersService));
                this.groups.put(room.getName(), groupActor);
            }

            groupActor.tell(new RegisterGroupUser(msg.getUsername()), getSender());
        });

        users.forEach(user -> {
            PrivateDTO priv = new PrivateDTO(user.getUsername(), msg.getUsername());
            ActorRef privActor = this.privates.get(priv);

            if (privActor == null) {
                privActor = getContext().actorOf(Private.props(priv, clientsService, mappersService));
                this.privates.put(priv, privActor);
            }

            privActor.tell(new RegisterPrivateUser(msg.getUsername()), getSender());
        });

        noAuthorizedConnections.remove(getSender());
        authorizedConnections.put(msg.getUsername(), getSender());
    }

    private void onRegisterConnectionRequest(RegisterConnection msg) {
        noAuthorizedConnections.add(getSender());
        getSender().tell(new ConnectionSuccessful(), ActorRef.noSender());
    }

    private void onPrivateSendMessageRequest(PrivateSendMessageRequest msg) {
        clientsService.getAuth().addUserToUser(msg.getJwtTokenDTO().getAccessToken(), msg.getReceiver());
        PrivateDTO privateDTO = new PrivateDTO(msg.getReceiver(), msg.getSender());

        ActorRef receiverActor = authorizedConnections.get(msg.getReceiver());
        ActorRef privateActor = getContext().actorOf(Private.props(privateDTO, clientsService, mappersService));

        privateActor.tell(
                receiverActor != null ? RegisterUserWithNotify.builder()
                        .senderActor(getSender())
                        .sender(msg.getSender())
                        .receiverActor(receiverActor)
                        .receiver(msg.getReceiver())
                        .build()
                        :
                        RegisterUserWithNotify.builder()
                        .senderActor(getSender())
                        .sender(msg.getSender())
                        .build(), ActorRef.noSender()
        );

        privateActor.tell(msg, getSender());
    }

    private void onSearchResponse(SearchResponse msg) {
        msg.getUsers().stream().filter(e -> authorizedConnections.get(e.getUsername()) != null).forEach(e -> e.setActive(true));
        getSender().tell(msg, ActorRef.noSender());
    }

    private void onLogout(Logout msg) {
        int o = noAuthorizedConnections.size();
        noAuthorizedConnections.remove(getSender());
        o = authorizedConnections.size();
        authorizedConnections.remove(msg.getUsername());
    }
}
