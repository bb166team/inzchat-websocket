package pl.bb166.inzchat.websocket.configuration;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.bb166.inzchat.websocket.client.Auth;
import pl.bb166.inzchat.websocket.client.Message;

@Configuration
public class FeignClientConfiguration {
    @Value("${inzChat.websocket.component.auth.proxyUrl}")
    private String authProxy;

    @Value("${inzChat.websocket.component.auth.proxyPort}")
    private String authPort;

    @Value("${inzChat.websocket.component.message.proxyUrl}")
    private String messageProxy;

    @Value("${inzChat.websocket.component.message.proxyPort}")
    private String messagePort;

    @Bean
    public Auth auth() {
        return Feign.builder()

                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(Auth.class, "http://" + authProxy + ":" + authPort);
    }

    @Bean
    public Message message() {
        return Feign.builder()
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(Message.class, "http://" + messageProxy + ":" + messagePort);
    }
}
