package pl.bb166.inzchat.websocket.configuration;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.bb166.inzchat.websocket.actor.Root;
import pl.bb166.inzchat.websocket.service.*;

import java.net.InetSocketAddress;

@Configuration
public class BootstrapConfiguration {
    static Logger logger = LoggerFactory.getLogger(BootstrapConfiguration.class);
    @Value("${inzChat.websocket.server.port}")
    private int port;

    @Value("${inzChat.websocket.component.auth.clientId}")
    private String clientId;

    @Value("${inzChat.websocket.component.auth.clientSecret}")
    private String clientSecret;

    @Bean
    public OkHttpClient jwtClient() {
        return new OkHttpClient
                .Builder()
                .addInterceptor(new BasicAuthInterceptor(clientId, clientSecret))
                .build();
    }

    @Bean
    public ActorSystem actorSystem() {
        return ActorSystem.apply();
    }

    @Bean
    public ActorRef rootActor(ActorSystem actorSystem, ClientsService clientsService, MappersService mappersService) {
        return actorSystem.actorOf(Root.props(clientsService, mappersService));
    }

    @Bean(destroyMethod = "stop")
    public WebSocketServer webSocketServer(ActorSystem actorSystem,
                                           ActorRef root,
                                           MessageConverterService messageConverterService,
                                           ClientsService clientsService,
                                           MappersService mappersService) {
        WebSocketServer webSocketServer =  new WebsocketService(
                new InetSocketAddress(port),
                actorSystem,
                root,
                messageConverterService,
                clientsService,
                mappersService
        );
        webSocketServer.start();
        return webSocketServer;
    }

    @Bean
    public Gson gson() {
        return new Gson();
    }
}