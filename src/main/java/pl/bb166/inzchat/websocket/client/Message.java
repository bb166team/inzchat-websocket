package pl.bb166.inzchat.websocket.client;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import pl.bb166.inzchat.websocket.domain.dto.CreateGroupMessageDTO;
import pl.bb166.inzchat.websocket.domain.dto.CreatePrivateMessageDTO;
import pl.bb166.inzchat.websocket.domain.dto.MessageDTO;

import java.util.List;

public interface Message {
    @RequestLine("GET /message/{groupName}")
    @Headers("Authorization: Bearer {jwtToken}")
    List<MessageDTO> getMessagesForGroup(@Param("jwtToken") String jwtToken, @Param("groupName") String groupName);

    @RequestLine("GET /message/{firstUser}/{secondUser}")
    @Headers("Authorization: Bearer {jwtToken}")
    List<MessageDTO> getPrivateMessagesForUsers(
            @Param("jwtToken") String jwtToken,
            @Param("firstUser") String firstUser,
            @Param("secondUser") String secondUser
    );

    @RequestLine("POST /message/group")
    @Headers({"Content-Type: application/json",
            "Authorization: Bearer {jwtToken}"})
    void createGroupMessage(@Param("jwtToken") String jwtToken, CreateGroupMessageDTO messageDTO);

    @RequestLine("POST /message/private")
    @Headers({"Content-Type: application/json",
            "Authorization: Bearer {jwtToken}"})
    void createPrivateMessage(@Param("jwtToken") String jwtToken, CreatePrivateMessageDTO messageDTO);
}
