package pl.bb166.inzchat.websocket.client;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import pl.bb166.inzchat.websocket.domain.dto.RegisterUserDTO;
import pl.bb166.inzchat.websocket.domain.dto.RegisterUserValidationDTO;
import pl.bb166.inzchat.websocket.domain.dto.RoomDTO;
import pl.bb166.inzchat.websocket.domain.dto.UserDTO;

import java.util.List;

public interface Auth {
    @RequestLine("POST /register")
    @Headers("Content-Type: application/json")
    RegisterUserValidationDTO registerNewUser(RegisterUserDTO registerUserDTO);

    @RequestLine("PUT /room/{groupName}")
    @Headers("Authorization: Bearer {jwtToken}")
    void addUserToGroup(@Param("jwtToken") String jwtToken, @Param("groupName") String groupName);

    @RequestLine("GET /room")
    @Headers("Authorization: Bearer {jwtToken}")
    List<RoomDTO> rooms(@Param("jwtToken") String jwtToken);

    @RequestLine("GET /user")
    @Headers("Authorization: Bearer {jwtToken}")
    List<UserDTO> getAllUsers(@Param("jwtToken") String jwtToken);

    @RequestLine("GET /room/{query}")
    @Headers("Authorization: Bearer {jwtToken}")
    List<RoomDTO> searchRooms(@Param("jwtToken") String jwtToken, @Param("query") String query);

    @RequestLine("GET /user/{query}")
    @Headers("Authorization: Bearer {jwtToken}")
    List<UserDTO> searchUsers(@Param("jwtToken") String jwtToken, @Param("query") String query);

    @RequestLine("POST /user/{username}")
    @Headers("Authorization: Bearer {jwtToken}")
    List<UserDTO> addUserToUser(@Param("jwtToken") String jwtToken, @Param("username") String username);

    @RequestLine("POST /room/{groupName}")
    @Headers("Authorization: Bearer {jwtToken}")
    void addGroup(@Param("jwtToken") String jwtToken, @Param("groupName") String groupName);

    @RequestLine("PUT /room/{groupName}/{username}")
    @Headers("Authorization: Bearer {jwtToken}")
    void addAnotherUserToGroup(@Param("jwtToken") String jwtToken,
                               @Param("groupName") String groupName,
                               @Param("username") String username);
}