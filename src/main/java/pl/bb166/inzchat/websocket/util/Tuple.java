package pl.bb166.inzchat.websocket.util;

public class Tuple<A, B> {
    public final A _1;
    public final B _2;

    private Tuple(A _1, B _2) {
        this._1 = _1;
        this._2 = _2;
    }

    public static <A, B> Tuple<A, B> tuple(A _1, B _2) {
        return new Tuple<>(_1, _2);
    }
}
