package pl.bb166.inzchat.websocket.domain.message;

import lombok.Getter;
import lombok.Setter;

public class Logout{

    @Getter @Setter
    private String username;
}
