package pl.bb166.inzchat.websocket.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class CreatePrivateMessageDTO {

    @Getter
    @Setter
    private String firstUser;

    @Getter @Setter
    private String secondUser;

    @Getter @Setter
    private String message;

    @Getter @Setter
    private MessageType messageType;
}
