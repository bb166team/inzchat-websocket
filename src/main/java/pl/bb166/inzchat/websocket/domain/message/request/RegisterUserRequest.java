package pl.bb166.inzchat.websocket.domain.message.request;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.message.Message;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserRequest extends Message {

    @Getter @Setter
    private String username;

    @Getter @Setter
    private String password;

    @Getter @Setter
    private String email;
}
