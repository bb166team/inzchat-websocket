package pl.bb166.inzchat.websocket.domain.message.request;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.message.Message;

@ToString
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest extends Message {
    @Getter @Setter
    private String username;
    @Getter @Setter
    private String password;
}
