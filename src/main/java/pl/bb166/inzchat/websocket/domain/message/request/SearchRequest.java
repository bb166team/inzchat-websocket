package pl.bb166.inzchat.websocket.domain.message.request;

import lombok.Getter;
import lombok.Setter;
import pl.bb166.inzchat.websocket.domain.message.Message;

public class SearchRequest extends Message {
    @Getter @Setter
    private String query;
}
