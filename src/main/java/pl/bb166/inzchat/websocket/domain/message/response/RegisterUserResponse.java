package pl.bb166.inzchat.websocket.domain.message.response;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.ValidationError;
import pl.bb166.inzchat.websocket.domain.message.Message;

import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterUserResponse extends Message {
    @Getter @Setter
    private Set<ValidationError> errors;
}
