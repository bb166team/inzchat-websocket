package pl.bb166.inzchat.websocket.domain.message.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.bb166.inzchat.websocket.domain.message.Message;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class UploadFileResponse extends Message {
    @Getter @Setter
    private String token;

    @Getter @Setter
    private List<String> owners;
}