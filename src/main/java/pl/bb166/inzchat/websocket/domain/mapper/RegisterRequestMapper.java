package pl.bb166.inzchat.websocket.domain.mapper;

import org.mapstruct.Mapper;
import pl.bb166.inzchat.websocket.domain.dto.RegisterUserDTO;
import pl.bb166.inzchat.websocket.domain.message.request.RegisterUserRequest;

@Mapper(componentModel = "spring")
public interface RegisterRequestMapper {
    RegisterUserDTO registerRequestToRegisterUserDTO(RegisterUserRequest registerUserRequest);
}
