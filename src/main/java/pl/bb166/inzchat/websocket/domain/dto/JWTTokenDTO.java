package pl.bb166.inzchat.websocket.domain.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString
public class JWTTokenDTO {
    @Getter @Setter
    @SerializedName("access_token")
    private String accessToken;

    @Getter @Setter
    @SerializedName("token_type")
    private String tokenType;

    @Getter @Setter
    @SerializedName("refresh_token")
    private String refreshToken;

    @Getter @Setter
    @SerializedName("expires_in")
    private Integer expiresIn;

    @Getter @Setter
    @SerializedName("scope")
    private String scope;

    @Getter @Setter
    @SerializedName("jti")
    private String jti;
}
