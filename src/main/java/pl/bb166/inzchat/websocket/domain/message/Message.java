package pl.bb166.inzchat.websocket.domain.message;

import lombok.*;

@NoArgsConstructor
public class Message {
    @Getter @Setter
    private String messageType;

    @Getter @Setter
    private String componentName;
}