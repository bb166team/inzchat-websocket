package pl.bb166.inzchat.websocket.domain.message.request;

import lombok.Getter;
import lombok.Setter;
import pl.bb166.inzchat.websocket.domain.dto.JWTTokenDTO;
import pl.bb166.inzchat.websocket.domain.dto.MessageType;
import pl.bb166.inzchat.websocket.domain.message.Message;


public class PrivateSendMessageRequest extends Message {
    @Getter @Setter
    private String message;

    @Getter @Setter
    private String receiver;

    @Getter @Setter
    private String sender;

    @Getter @Setter
    private JWTTokenDTO jwtTokenDTO;

    @Getter @Setter
    private MessageType type;
}
