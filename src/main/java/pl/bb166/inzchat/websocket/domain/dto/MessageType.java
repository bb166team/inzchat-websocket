package pl.bb166.inzchat.websocket.domain.dto;

public enum MessageType {
    TEXT, FILE
}
