package pl.bb166.inzchat.websocket.domain.message.response;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.dto.MessageDTO;
import pl.bb166.inzchat.websocket.domain.message.Message;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageResponse extends Message {
    @Getter @Setter
    private String groupName;
    @Getter @Setter
    private List<MessageDTO> messages;
    @Getter @Setter
    private Position position;
}
