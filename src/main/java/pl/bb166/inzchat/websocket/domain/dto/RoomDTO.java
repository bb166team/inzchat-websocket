package pl.bb166.inzchat.websocket.domain.dto;

import lombok.*;

@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RoomDTO {
    @Getter
    @Setter
    private String name;
}
