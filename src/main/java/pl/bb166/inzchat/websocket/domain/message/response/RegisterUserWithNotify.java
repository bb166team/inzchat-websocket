package pl.bb166.inzchat.websocket.domain.message.response;

import akka.actor.ActorRef;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserWithNotify {
    @Getter @Setter
    private String receiver;

    @Getter @Setter
    private String sender;

    @Getter @Setter
    private ActorRef receiverActor;

    @Getter @Setter
    private ActorRef senderActor;
}
