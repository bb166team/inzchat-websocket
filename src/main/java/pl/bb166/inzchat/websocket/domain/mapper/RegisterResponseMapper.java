package pl.bb166.inzchat.websocket.domain.mapper;

import org.mapstruct.Mapper;
import pl.bb166.inzchat.websocket.domain.dto.RegisterUserValidationDTO;

import pl.bb166.inzchat.websocket.domain.message.response.RegisterUserResponse;

@Mapper(componentModel = "spring")
public interface RegisterResponseMapper {
    RegisterUserResponse registerUserValidationDTOToRegisterRequest(RegisterUserValidationDTO registerUserValidationDTO);
}