package pl.bb166.inzchat.websocket.domain.message.response;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.message.Message;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse extends Message {
    public enum LoginStatus {SUCCESSFUL, FILED}

    @Getter @Setter
    private LoginStatus loginStatus;
}
