package pl.bb166.inzchat.websocket.domain.message.request;

public enum Type {
    PRIVATE, GROUP
}
