package pl.bb166.inzchat.websocket.domain.message;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.PrivateDTO;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserRegisteredInPrivate {
    @Getter @Setter
    private PrivateDTO privateDTO;
}
