package pl.bb166.inzchat.websocket.domain.message.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.bb166.inzchat.websocket.domain.dto.RoomDTO;
import pl.bb166.inzchat.websocket.domain.dto.UserDTO;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class LoginSuccessfulResponse {
    @Getter @Setter
    private String username;

    @Getter @Setter
    private List<UserDTO> users;

    @Getter @Setter
    private List<RoomDTO> rooms;
}
