package pl.bb166.inzchat.websocket.domain.message;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegisterGroupUser {
    @Getter @Setter
    private String groupName;
}
