package pl.bb166.inzchat.websocket.domain.dto;

import lombok.*;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CreateGroupMessageDTO {
    @Getter @Setter
    private String groupName;

    @Getter @Setter
    private String message;

    @Getter @Setter
    private MessageType messageType;
}