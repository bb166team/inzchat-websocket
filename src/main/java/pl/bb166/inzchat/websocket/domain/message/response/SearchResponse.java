package pl.bb166.inzchat.websocket.domain.message.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.bb166.inzchat.websocket.domain.dto.RoomDTO;
import pl.bb166.inzchat.websocket.domain.dto.UserDTO;
import pl.bb166.inzchat.websocket.domain.message.Message;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class SearchResponse extends Message {
    @Getter @Setter
    List<RoomDTO> rooms;
    @Getter @Setter
    List<UserDTO> users;
}
