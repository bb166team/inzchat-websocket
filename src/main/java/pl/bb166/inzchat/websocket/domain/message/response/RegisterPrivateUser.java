package pl.bb166.inzchat.websocket.domain.message.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.bb166.inzchat.websocket.domain.message.Message;

@NoArgsConstructor
@AllArgsConstructor
public class RegisterPrivateUser extends Message {
    @Getter @Setter
    private String username;
}
