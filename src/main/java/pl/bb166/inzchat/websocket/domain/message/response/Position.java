package pl.bb166.inzchat.websocket.domain.message.response;

public enum Position {
    END, START
}
