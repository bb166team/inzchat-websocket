package pl.bb166.inzchat.websocket.domain.message.request;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.dto.JWTTokenDTO;
import pl.bb166.inzchat.websocket.domain.dto.MessageType;
import pl.bb166.inzchat.websocket.domain.message.Message;

@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GroupSendMessageRequest extends Message {

    @Getter @Setter
    private String groupName;

    @Getter @Setter
    private String author;

    @Getter @Setter
    private String content;

    @Getter @Setter
    private JWTTokenDTO jwtTokenDTO;

    @Getter @Setter
    private MessageType type;
}