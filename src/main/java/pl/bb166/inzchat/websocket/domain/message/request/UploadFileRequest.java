package pl.bb166.inzchat.websocket.domain.message.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.bb166.inzchat.websocket.domain.message.Message;

@NoArgsConstructor
@AllArgsConstructor
public class UploadFileRequest extends Message {

    @Getter @Setter
    private String name;

    @Getter @Setter
    private String groupType;
}
