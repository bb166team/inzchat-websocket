package pl.bb166.inzchat.websocket.domain;

import lombok.*;

@EqualsAndHashCode
public class PrivateDTO {
    @Getter
    private String firstUser;

    @Getter
    private String secondUser;

    public PrivateDTO(String firstUser, String secondUser) {
        int res = firstUser.compareTo(secondUser);
        this.firstUser = res < 0 ? firstUser : secondUser;
        this.secondUser = res > 0 ? firstUser : secondUser;
    }
}
