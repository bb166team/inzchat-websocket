package pl.bb166.inzchat.websocket.domain.message.response;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.message.Message;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DownloadFileResponse extends Message {
    @Getter @Setter
    private String token;
    @Getter @Setter
    private Long id;
}
