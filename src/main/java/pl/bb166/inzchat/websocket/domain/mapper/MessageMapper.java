package pl.bb166.inzchat.websocket.domain.mapper;


import org.mapstruct.*;
import pl.bb166.inzchat.websocket.domain.dto.CreateGroupMessageDTO;
import pl.bb166.inzchat.websocket.domain.dto.CreatePrivateMessageDTO;
import pl.bb166.inzchat.websocket.domain.dto.MessageDTO;
import pl.bb166.inzchat.websocket.domain.message.request.GroupSendMessageRequest;
import pl.bb166.inzchat.websocket.domain.message.request.PrivateSendMessageRequest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel =  "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class MessageMapper {
    protected static DateTimeFormatter FORMATTER =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Mappings({
        @Mapping(target = "message", source = "content"),
            @Mapping(target = "messageType", source = "type")
    })
    public abstract CreateGroupMessageDTO groupSendMessageRequestToCreateGroupMessageDTO(GroupSendMessageRequest groupSendMessageRequest);

    @Mappings({
            @Mapping(target = "message", source = "content"),
            @Mapping(target = "messageType", source = "type")
    })
    public abstract MessageDTO groupSendMessageRequestToMessageDTO(GroupSendMessageRequest groupSendMessageRequest);

    @Mappings({
        @Mapping(source = "receiver", target = "firstUser"),
        @Mapping(source = "sender", target = "secondUser"),
            @Mapping(source = "type", target = "messageType")
    })
    public abstract CreatePrivateMessageDTO privateSendMessageRequestToCreatePrivateMessageDTO(PrivateSendMessageRequest privateSendMessageRequest);

    @Mappings({
            @Mapping(source = "sender", target = "author"),
            @Mapping(source = "type", target = "messageType")
    })
    public abstract MessageDTO privateSendMessageRequestToMessageDTO(PrivateSendMessageRequest privateSendMessageRequest);

    @AfterMapping
    protected void now(@MappingTarget MessageDTO messageDTO) {
        messageDTO.setDate(LocalDateTime.now().format(FORMATTER));
    }
}
