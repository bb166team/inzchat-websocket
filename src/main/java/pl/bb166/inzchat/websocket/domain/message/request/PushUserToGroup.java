package pl.bb166.inzchat.websocket.domain.message.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.bb166.inzchat.websocket.domain.message.Message;

@AllArgsConstructor
@NoArgsConstructor
public class PushUserToGroup extends Message {
    @Getter @Setter
    private String username;
    @Getter @Setter
    private String groupName;
}
