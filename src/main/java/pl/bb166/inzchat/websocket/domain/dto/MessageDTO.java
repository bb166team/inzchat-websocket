package pl.bb166.inzchat.websocket.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class MessageDTO {
    @Getter @Setter
    private String author;

    @Getter @Setter
    private String message;

    @Getter @Setter
    private String date;

    @Getter @Setter
    private MessageType messageType;
}