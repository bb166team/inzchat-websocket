package pl.bb166.inzchat.websocket.domain.dto;

import lombok.*;
import pl.bb166.inzchat.websocket.domain.ValidationError;
import pl.bb166.inzchat.websocket.domain.message.Message;

import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RegisterUserValidationDTO extends Message {

    @Getter
    private Set<ValidationError> errors;
}