package pl.bb166.inzchat.websocket.domain.dto;

import lombok.*;

@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    @Getter @Setter
    private String username;

    @Getter @Setter
    private Boolean active;
}
