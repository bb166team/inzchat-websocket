package pl.bb166.inzchat.websocket.domain.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class UserRegisteredInGroup {
    @Getter @Setter
    private String name;
}
